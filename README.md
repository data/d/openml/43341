# OpenML dataset: Diabetes-Dataset-2019

https://www.openml.org/d/43341

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was collected by Neha Prerna Tigga and Dr. Shruti Garg of the Department of Computer Science and Engineering, BIT Mesra, Ranchi-835215 for research, non-commercial purposes only. An article is also published implementing this dataset. For more information and citation of this dataset please refer: 
Tigga, N. P.,  Garg, S. (2020). Prediction of Type 2 Diabetes using Machine Learning Classification Methods. Procedia Computer Science, 167, 706-716. DOI: https://doi.org/10.1016/j.procs.2020.03.336
Content
There is a total of 952 instances with 17 independent predictor variables and one binary target or dependent variable, Diabetes. 
Acknowledgements
We would like to thank all the participants who contributed towards the building of this dataset.
Inspiration
To build a machine learning algorithm to predict if a person has diabetes or not?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43341) of an [OpenML dataset](https://www.openml.org/d/43341). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43341/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43341/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43341/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

